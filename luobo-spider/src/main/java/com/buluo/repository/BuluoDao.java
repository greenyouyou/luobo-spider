package com.buluo.repository;

import org.springframework.stereotype.Repository;

import com.luobo.entity.Guanka;
import com.luobo.entity.News;
import com.luobo.entity.Tujian;
import com.luobo.util.LuoboJdbcDaoSupport;

@Repository
public class BuluoDao  extends LuoboJdbcDaoSupport{

	public void saveNews(News news){
		
		String sql_query = "select count(id) from buluo_news where log_id = ?";
		int count = getJdbcTemplate().queryForObject(sql_query, new Object[]{news.getShow()},Integer.class);
		
		if(count > 0 ){
			return;
		}
		
		String sql = "insert into buluo_news(game,title,create_date,content,icon,log_id) values(?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{news.getGame(),news.getTitle(),news.getDate(),news.getContent(),news.getIcon(),news.getShow()});
	}

	public void saveGuanka(Guanka news) {
		String sql_query = "select count(id) from buluo_guanka_text where log_id = ?";
		int count = getJdbcTemplate().queryForObject(sql_query, new Object[]{news.getShow()},Integer.class);
		
		if(count > 0 ){
			return;
		}
		
		String sql = "insert into buluo_guanka_text(game,title,create_date,content,icon,log_id) values(?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{news.getGame(),news.getTitle(),news.getDate(),news.getContent(),news.getIcon(),news.getShow()});		
	}

	public void saveData(Tujian tujian) {
		String sql = "insert into buluo_tujian(game,type,name,title,create_date,content) values(?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{tujian.getGame(),tujian.getType(),tujian.getName(),tujian.getTitle(),tujian.getDate(),tujian.getContent()});
	}

	public void saveQuestion(News news) {
		String sql_query = "select count(id) from buluo_question where log_id = ?";
		int count = getJdbcTemplate().queryForObject(sql_query, new Object[]{news.getShow()},Integer.class);
		
		if(count > 0 ){
			return;
		}
		
		String sql = "insert into buluo_question(game,title,create_date,content,icon,log_id) values(?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{news.getGame(),news.getTitle(),news.getDate(),news.getContent(),news.getIcon(),news.getShow()});		
	}

	public void saveGuankaVideo(Guanka guanka) {
		String sql_query = "select count(id) from buluo_guanka_video where log_id = ?";
		int count = getJdbcTemplate().queryForObject(sql_query, new Object[]{guanka.getShow()},Integer.class);
		
		if(count > 0 ){
			return;
		}
		
		String sql = "insert into buluo_guanka_video(game,title,create_date,content,icon,log_id,video_url) values(?,?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{guanka.getGame(),guanka.getTitle(),guanka.getDate(),guanka.getContent(),guanka.getIcon(),guanka.getShow(),guanka.getVideoUrl()});		
	}
}
