package com.buluo.pipeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.buluo.repository.BuluoDao;
import com.luobo.entity.Tujian;

@Component
public class DataPipeLine implements Pipeline {
	

	@Resource
	private BuluoDao buluoDao;

	@Override
	public void process(ResultItems map, Task task) {
		String title = map.get("title");
		if(title == null){return;}
		String content = map.get("content");
		
		
		String name = title.replace("我的部落", "");
		
		String dateStr = map.get("date");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
		}
		
		Tujian tujian = new Tujian();
		tujian.setGame(3);
		tujian.setType(4);
		tujian.setName(name);
		tujian.setDate(date);
		tujian.setTitle(title);
		tujian.setContent(content);		
		
		buluoDao.saveData(tujian);
		
		System.out.println("我的部落【" + title + "】数据已经录入。");
	}

	
}
