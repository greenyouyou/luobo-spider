package com.buluo.pipeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.buluo.repository.BuluoDao;
import com.luobo.entity.Guanka;

@Component
public class ShipinPipeLine implements Pipeline {

	@Resource
	private BuluoDao buluoDao;
	
	@Override
	public void process(ResultItems map, Task task) {
    	String title = map.get("title");
    	if(title == null){return;}
    	String content = map.get("content");
    	
    	String dateStr = map.get("date");
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    	Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
		}
    	
		String show = map.get("id");
		
    	Guanka guanka = new Guanka();
    	guanka.setGame(3);
    	guanka.setDate(date);
    	guanka.setTitle(title);
    	
    	String regexVideo = "http://player.youku.com\\S*v.swf";
		Pattern pVideo = Pattern.compile(regexVideo);
		Matcher mVideo = pVideo.matcher(content);
		String newUrl = "";
		if(mVideo.find()){
			String url = mVideo.group(0);
			url = url.replace("http://player.youku.com/player.php/sid/", "");
			url = url.replace("/v.swf", "");
			newUrl = "http://v.youku.com/player/getRealM3U8/vid/" + url + "/type/mp4/v.m3u8";
		}
		content = content.replaceAll("<p.*</object></p>", "");
    	
		guanka.setVideoUrl(newUrl);
		guanka.setContent(content);		
    	guanka.setShow(show);
    	
    	String regex = "http://img1.gamedog.cn\\S*(.jpg)|(.png)";
    	Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(content);
		String imgUrl = "";
		if(m.find()){
			imgUrl = m.group(0).replace("img1", "mimg1");
		}
		guanka.setIcon(imgUrl);
    	
    	buluoDao.saveGuankaVideo(guanka);
    
    	System.out.println("【" + guanka.getTitle() + "】爬取完成");
	}

}
