package com.luobo.core.pipeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.luobo.entity.Guanka;
import com.luobo.repository.LuoboDao;

@Component("guankaPipeLine")
public class GuankaPipeLine implements Pipeline {

	@Resource
	private LuoboDao luoboDao;
	
	@Override
	public void process(ResultItems map, Task task) {
    	String title = map.get("title");
    	if(title == null){return;}
    	String content = map.get("content");
    	
//    	if(title.indexOf("第") != -1){
//    		return;
//    	}
    	
//    	int end = title.indexOf("金");
//    	
//    	String levelStr = title.substring(title.indexOf("战")+1,end );
    	
    	String regEx="[^0-9]";   
    	Pattern p = Pattern.compile(regEx);   
    	Matcher m = p.matcher(title);
    	String levelStr = m.replaceAll("").trim();
    	
    	String dateStr = map.get("date");
    	int level = Integer.parseInt(levelStr);
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    	Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
		}
    	
    	Guanka guanka = new Guanka();
    	guanka.setGame(1);
    	guanka.setType(0);//记得改这里
		guanka.setDate(date);
    	guanka.setTitle(title);
    	guanka.setLevel(level);
    	guanka.setContent(content);		
    	
    	//luoboDao.saveGuanka(guanka);
    	
    	System.out.println("保卫萝卜2第 " + level + "关数据已经录入。");
    	
	}

}
