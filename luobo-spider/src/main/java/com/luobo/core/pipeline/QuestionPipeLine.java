package com.luobo.core.pipeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.luobo.entity.Question;
import com.luobo.repository.LuoboDao;

@Component
public class QuestionPipeLine implements Pipeline {

	@Resource
	private LuoboDao luoboDao;
	
	@Override
	public void process(ResultItems map, Task task) {
    	String title = map.get("title");
    	if(title == null){return;}
    	String content = map.get("content");
    	
    	String dateStr = map.get("date");
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    	Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
		}
    	
		String show = map.get("id");
		
		Question question = new Question();
		question.setGame(2);
		question.setDate(date);
		question.setTitle(title);
		question.setContent(content);		
		question.setShow(show);
    	
    	luoboDao.saveQuestion(question);
    
    	System.out.println("【" + question.getTitle() + "】爬取完成");
	}

}
