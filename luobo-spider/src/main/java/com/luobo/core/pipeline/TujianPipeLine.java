package com.luobo.core.pipeline;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.luobo.entity.Tujian;
import com.luobo.repository.LuoboDao;

@Component
public class TujianPipeLine implements Pipeline {
	

	@Resource
	private LuoboDao luoboDao;

	@Override
	public void process(ResultItems map, Task task) {
		String title = map.get("title");
		if(title == null){return;}
		String content = map.get("content");
		
		
		String name = "";
//		int end = title.indexOf("场景");
//		int flag = title.indexOf("之");
//		if(flag != -1){
//			name = title.substring(flag+1,title.length());
//		}else{
//			name =title.substring(title.indexOf("2")+1,end);
//		}
		name="冰冻星星";
		
		String dateStr = map.get("date");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
		}
		
		Tujian tujian = new Tujian();
		tujian.setGame(1);
		tujian.setType(2);
		tujian.setName(name);
		tujian.setDate(date);
		tujian.setTitle(title);
		tujian.setContent(content);		
		
		luoboDao.saveTujian(tujian);
		
		System.out.println("保卫萝卜【" + title + "】数据已经录入。");
	}

	
}
