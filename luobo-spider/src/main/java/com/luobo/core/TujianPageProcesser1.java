package com.luobo.core;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * 保卫萝卜2
 * @author guolei
 *
 */
@Component
public class TujianPageProcesser1  implements PageProcessor  {

    @Override
    public void process(Page page) {
        List<String> links = page.getHtml().links().regex("http://baoweiluobo\\.gamedog\\.cn/tujian/\\d+/\\d+\\.html").all();
        List<String> list = new ArrayList<String>();
        list.add("http://baoweiluobo.gamedog.cn/tujian/20130504/144163.html");
        page.addTargetRequests(list);
        page.putField("title", page.getHtml().xpath("//div[@class='main1']/div[@class='main1_left']/div[@class='wen']/h1/text()").toString());
        page.putField("content", page.getHtml().xpath("//div[@class='main1']/div[@class='main1_left']/div[@class='wen']/div[@class='news_neirong']/html()").toString());
        page.putField("date", page.getHtml().xpath("//div[@class='main1']/div[@class='main1_left']/div[@class='wen']/div[@class='newsa']/span/text()").toString());
    }

    @Override
    public Site getSite() {
        return Site.me().setDomain("baoweiluobo.gamedog.cn").
                setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");
    }
	
}
