package com.luobo.util;

import javax.annotation.PostConstruct;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import javax.annotation.Resource;
import javax.sql.DataSource;

public class LuoboJdbcDaoSupport extends JdbcDaoSupport{

	@Resource(name = "dataSource") 
	private DataSource myDataSource;

	@PostConstruct
	public void injectDataSource() {
		super.setDataSource(myDataSource);
	}
}
