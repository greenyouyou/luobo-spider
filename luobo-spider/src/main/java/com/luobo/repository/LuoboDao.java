package com.luobo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.luobo.entity.Guanka;
import com.luobo.entity.News;
import com.luobo.entity.Question;
import com.luobo.entity.Tujian;
import com.luobo.util.LuoboJdbcDaoSupport;

@Repository
public class LuoboDao extends LuoboJdbcDaoSupport {

	public void saveGuanka(Guanka guanka){
		String sql = "insert into luobo_guanka_text(game,type,level,title,create_date,icon,content) values(?,?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{guanka.getGame(),guanka.getType(),guanka.getLevel(),guanka.getTitle(),guanka.getDate(),guanka.getIcon(),guanka.getContent()});
	}
	
	public void saveNews(News news){
		
		String sql_query = "select count(id) from luobo_news where log_id = ?";
		int count = getJdbcTemplate().queryForObject(sql_query, new Object[]{news.getShow()},Integer.class);
		
		if(count > 0 ){
			return;
		}
		
		String sql = "insert into luobo_news(game,title,create_date,content,log_id) values(?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{news.getGame(),news.getTitle(),news.getDate(),news.getContent(),news.getShow()});
	}

	public void saveTujian(Tujian tujian) {
		String sql = "insert into luobo_tujian(game,type,name,title,create_date,content) values(?,?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{tujian.getGame(),tujian.getType(),tujian.getName(),tujian.getTitle(),tujian.getDate(),tujian.getContent()});
	}

	public void saveQuestion(Question question) {
		String sql = "insert into luobo_question(game,title,create_date,content,log_id) values(?,?,?,?,?)";
		getJdbcTemplate().update(sql,new Object[]{question.getGame(),question.getTitle(),question.getDate(),question.getContent(),question.getShow()});
	}
	
	public List<Guanka> getGuanKaVideoList(int game){
		String sql = "select id,content from luobo_guanka_video where game = ?";
		return getJdbcTemplate().query(sql, new Object[]{game},new RowMapper<Guanka>(){
			@Override
			public Guanka mapRow(ResultSet rs, int rowNum) throws SQLException {
				Guanka guanka = new Guanka();
				guanka.setId(rs.getInt("id"));
				guanka.setContent(rs.getString("content"));
				return guanka;
			}
			
		});
	}
	
	public void updateGuankaVideo(int id,String content,String videoUrl){
		String sql = "update luobo_guanka_video set  content = ?,video_url=? where id = ?";
		getJdbcTemplate().update(sql,new Object[]{content,videoUrl,id});
	}
}
