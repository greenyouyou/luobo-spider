package com.luobo.schedule;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import us.codecraft.webmagic.Spider;

import com.buluo.BuluoGuanKaPageProcesser;
import com.buluo.NewsPageProcesser;
import com.buluo.WenTiPageProcesser;
import com.buluo.pipeline.BuluoGuanKaPipeLine;
import com.buluo.pipeline.NewsPipeLine;
import com.buluo.pipeline.WenTiPipeLine;
import com.luobo.core.NewsPageProcesser1;
import com.luobo.core.NewsPageProcesser2;
import com.luobo.core.pipeline.NewsPipeLine1;
import com.luobo.core.pipeline.NewsPipeLine2;

/**
 * 
 * Spring 与 Quartz 集成测试
 *
 * @author guolei
 * @version 1.0
 * @created 2013-10-30
 */
@Component
public class DataHandleService {

	private static Logger logger = LoggerFactory.getLogger(DataHandleService.class);
	
	@Resource
	private NewsPageProcesser1 newsPageProcesser1;
	@Resource
	private NewsPageProcesser2 newsPageProcesser2;
	@Resource
	private NewsPipeLine2 newsPipeLine2;
	@Resource
	private NewsPipeLine1 newsPipeLine1;
	@Resource
	private BuluoGuanKaPageProcesser buluoGuanKaPageProcesser;
	@Resource
	private BuluoGuanKaPipeLine buluoGuanKaPipeLine;
	@Resource
	private NewsPageProcesser newsPageProcesser;
	@Resource
	private NewsPipeLine newsPipeLine;
	@Resource
	private WenTiPageProcesser wenTiPageProcesser;
	@Resource
	private WenTiPipeLine wenTiPipeLine;
	
	// 被Spring的Quartz MethodInvokingJobDetailFactoryBean反射执行
	public void executeByQuartzLocalJob() {
		logger.info("handle news data................................................");
		String url1 = "http://baoweiluobo.gamedog.cn/news";
		Spider.create(newsPageProcesser1).addPipeline(newsPipeLine1).addUrl(url1).run();
		
		String url2 = "http://baoweiluobo2.gamedog.cn/news";
		Spider.create(newsPageProcesser2).addPipeline(newsPipeLine2).addUrl(url2).run();
		
		buluoGuanKaPageProcesser.setFlag(false);
		String url3 = "http://wodebuluo.gamedog.cn/gonglue";
		Spider.create(buluoGuanKaPageProcesser).addPipeline(buluoGuanKaPipeLine).addUrl(url3).run();
		
		String url4 = "http://wodebuluo.gamedog.cn/news";
		Spider.create(newsPageProcesser).addPipeline(newsPipeLine).addUrl(url4).run();
		
		String url5 = "http://wodebuluo.gamedog.cn/wenda";
		Spider.create(wenTiPageProcesser).addPipeline(wenTiPipeLine).addUrl(url5).run();
	}
	
}
